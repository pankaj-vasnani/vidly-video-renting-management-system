const _ = require('lodash')

const {User, validate} = require('../models/user');

const express = require('express');

const bcrypt = require('bcrypt');

const router = express.Router();

const authorize = require('../middleware/auth')

router.get('/me', authorize, async (req, res) => {
    const user = await User.findById(req.user._id).select('-password')

    if(! user) res.status(400).json({error: 'data not found'})

    res.status(200).json({data: user})
})

router.post('/', async (req, res) => {
    try {
        // Validate the input data
        const validResult = validate(req.body);

        if(validResult.error) {
            res.status(400).json({error: validResult.error})
        }

        // Find if user already exists
        const userExist = await User.findOne({email: req.body.email})

        if(!userExist) {
            res.status(400).json({error: 'user already exists.'})
        }

        // Insert the data
        let user = new User(_.pick(req.body, ['name', 'email', 'password']))

        const salt = await bcrypt.genSalt(10)
        user.password = await bcrypt.hash(user.password, salt)

        const result = await user.save();

        const token = user.generateAuthToken()

        res.header('x-auth-token', token).status(200).json({data: _.pick(result, ['_id', 'name', 'email'])});
    } catch(err) {
        res.status(400).json({error: err.message})
    }
})

module.exports = router 