const express = require('express');
const { Rental, validate } = require('../models/rental')
const { Customer } = require('../models/customer')
const { Movie } = require('../models/movie')
const Fawn = require('fawn');
const mongoose = require('mongoose')

const authenticate = require('../middleware/auth')

Fawn.init(mongoose)

const app = express();

const router = express.Router();


router.get('/', (req, res) => {
    const rentals = Rental.find().sort({id: -1});

    res.status(200).json({data: rentals})
})

router.post('/', authenticate, async (req, res) => {
    try{
        // Validate the data
        const valResult = validate(req.body);
        
        if(valResult.error) {
            res.status(400).json({error: valResult.error})
        }

        // Find the customer data
        const customer = await Customer.findById(req.body.customerId)

        if(! customer) {
            res.status(400).json({error: 'customer data not found'})
        }

        // Find the movie data
        const movie = await Movie.findById(req.body.movieId)

        if(! movie) {
            res.status(400).json({error: 'movie data not found'})
        }

        if(movie.numberInStock == 0) {
            res.status(400).json({error: 'movie out of stock'})
        }

        // Insert the data
        let rentalData = new Rental({
            rentalPrice: req.body.rentalPrice,
            customer: {
                _id: customer._id,
                name: customer.name,
                email: customer.email,
                isGold: customer.isGold,
                phone: customer.phone,
            },
            movie: {
                _id: movie._id,
                title: movie.title,
                dailyRentalRate: movie.dailyRentalRate
            }
        })

        new Fawn.Task()
            .save('rentals', rentalData)
            .update('movies', {_id: movie._id}, {$inc: {numberInStock: -1}})
            .run()

        res.status(200).json({data: rentalData})
    } catch(err) {
        res.status(400).json({error: err.message})
    }
})

module.exports = router

