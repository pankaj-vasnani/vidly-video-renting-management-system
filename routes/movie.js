const express = require('express');

const { Movie, validateData } = require('../models/movie');
const { Genre } = require('../models/genre');

const router = express.Router();

const authenticate = require('../middleware/auth')

/**
 * Function to get all the movies 
 */
router.get('/', async (req, res) => {
    try{
        let movies = await Movie.find().sort({title: 1});
        
        res.status(200).json({data: movies});
    } catch(err) {
        res.status(400).json({error: "Error in getting all the documents "+ err.message});
    }
});

/**
 * Function to get the particular movie 
 */
router.get('/:id', (req, res) => {
    const movie = Movie.findById(req.params.id);

    if(! movie) {
        res.status(400).json({error: "data not found"});
    }

    res.status(200).json({data: movie});
});

/**
 * Function to create the movie
 */
router.post('/', authenticate, async (req, res) => {
    try{
        // Validate the data
        let result = validateData(req.body);

        if(result.error) {
            return res.status(400).json({error: result.error.details[0].message});
        }

        // Find the genre
        let genre = await Genre.findById(req.body.genreId);

        if(! genre) {
            res.status(400).json({error: 'genre data not found'})
        }

        // Save the data in the database
        let movie = new Movie({
            title: req.body.title,
            numberInStock: req.body.numberInStock,
            dailyRentalRate: req.body.dailyRentalRate,
            genre: {
                id: genre._id,
                name: genre.name,
                slug: genre.slug
            }
        });

        movie = await movie.save();

        res.status(200).json({data: movie});
    } catch(err) {
        res.status(400).json({error: err.message})
    }
});


/**
 * Function to update the movie
 */
router.put('/:id', authenticate, async (req, res) => {
    try {
        // Validate the data
        let result = validateData(req.body);

        if(result.error) {
            return res.status(400).json({error: result.error.details[0].message});
        }

        // Get the existing movie data
        let movie = await Movie.findById(req.params.id);

        if(! movie) {
            res.status(400).json({error: 'data not found'})
        }

        // Find the genre
        let genre = await Genre.findById(req.body.genreId);

        if(! genre) {
            res.status(400).json({error: 'genre data not found'})
        }

        let updateData = {
            title: req.body.title, 
            numberInStock: req.body.numberInStock, 
            dailyRentalRate: req.body.dailyRentalRate,
            genre: {
                id: genre._id,
                name: genre.name,
                slug: genre.slug
            }
        };

        // Update the movie
        const finalData = await Movie.findByIdAndUpdate(req.params.id, updateData) 

        res.status(200).json({data: finalData})
    } catch(err) {
        res.status(400).json({error: err.message})
    }
})

/**
 * Function to remove the movie
 */
router.delete('/:id', authenticate, async (req, res) => {
    try{
        // Get the data
        const movie = await Movie.findById(req.params.id);

        if(! movie) {
            res.status(400).json({error: 'data not found'})
        }
        // Remove the data

        const result = await Movie.findByIdAndDelete(req.params.id);

        res.status(200).json({data: result})
    } catch(err) {
        res.status(400).json({error: err.message})
    }
})

/**
 * Function to remove the movie
 */
module.exports = router;