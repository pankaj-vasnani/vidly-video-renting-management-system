const express = require('express');

const { Rental } = require('../models/rental');
const { Movie } = require('../models/movie');

const router = express.Router();

const authenticate = require('../middleware/auth')


const Joi = require('joi')

const validate = require('../middleware/validate')

// router.post('/', [authenticate, validate(validateData)], async (req, res) => {
router.post('/', [authenticate], async (req, res) => {

    let result = await Rental.lookup(req.body.customerId, req.body.movieId)

    if(! result)
        res.status(404).json({error: "Rental data not found"})

    if(result.dateReturned)
        res.status(400).json({error: 'Rental already processed'})

    result.return();

    await result.save()

    await Movie.update(
        {
            _id: result.movie._id
        }, 
        {
            $inc: 
                {numberInStock: 1}
        }
    )
   
    res.json({data: result});
});


function validateData(req) {
    // Validate the input data
    const schema = {
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required()
    }

    const result = Joi.validate(req, schema);

    return result;
}

module.exports = router
