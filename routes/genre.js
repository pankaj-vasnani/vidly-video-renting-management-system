const express = require('express');

const { Genre, validateData } = require('../models/genre');

const router = express.Router();

const auth = require('../middleware/auth')

const admin = require('../middleware/admin')

const validateObjectId = require('../middleware/validateObjectId')

router.get('/', async (req, res) => {
    // throw new Error('could not get the genres')
    
    let genres = await Genre.find().sort({ _id: -1});
    
    res.status(200).json({data: genres});
});

/**
 * Function to get the particular genre 
 */
router.get('/:id', validateObjectId, async (req, res) => {
    const genre = await Genre.findById(req.params.id);

    if(! genre) {
        res.status(400).json({error: "Data not found"});
    }

    res.status(200).json({data: genre});
});

/**
 * Function to save the new genre
 */
router.post('/', auth, async (req, res) => {
    // Validate the data
    let result = validateData(req.body);

    if(result.error) {
        return res.status(400).json({error: result.error.details[0].message});
    }

    // Save the data in the databawse
    let genre = await Genre.create({name: req.body.name, slug: req.body.slug});

    res.status(200).json({data: genre});
});

/**t
 * Function to update the existing genre
 */
router.put('/:id', auth, async (req, res) => {
    // Validate the data
    const valid = validateData(req.body);

    if(valid.error) {
        return res.status(400).json({error: valid.error.details[0].message});
    }

    // Check if existing data exists in the database
    const genre = await Genre.findById(req.params.id);

    if(! genre) {
        res.status(400).json({error: "Data not found"});
    }

    const result = await Genre.findByIdAndUpdate(req.params.id, {name: req.body.name, slug: req.body.slug}, {new: true});

    res.status(200).json({data: result});
});

/**
 * Function to delete the existing genre
 */
// router.delete('/:id', [auth, admin], async (req, res) => {
router.delete('/:id', [auth], async (req, res) => {
    // Check if existing data exists in the database
    const genre = await Genre.findById(req.params.id);

    if(! genre) {
        res.status(400).json({error: "Data not found"});
    }

    // Delete the document from the database
    const result = await Genre.findByIdAndDelete(req.params.id);

    // Return the deleted document
    res.status(200).json({data: result});
});

module.exports = router;