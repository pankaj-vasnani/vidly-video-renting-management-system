const { Customer, validateData } = require('../models/customer');

const express = require('express');

const router = express.Router();


/**
 * Function to get all the customers 
 */
router.get('/', async (req, res) => {
    try{
        let customers = await Customer.find().sort({ _id: -1});
        
        res.status(200).json({data: customers});
    } catch(err) {
        res.status(400).json({error: "Error in getting all the documents "+ err.message});
    }
});

/**
 * Function to get the particular customer 
 */
router.get('/:id', async (req, res) => {
    try{
        const customer = await Customer.findById(req.params.id);

        if(! customer) {
            res.status(400).json({error: "Data not found"});
        }

        res.status(200).json({data: customer});
    } catch(err) {
        res.status(400).json({error: "Error in getting the document "+ err.message});
    }
});

/**
 * Function to save the new customer
 */
router.post('/', async (req, res) => {
    try{
        console.log(req.body);

        // Validate the data
        let result = validateData(req.body);

        if(result.error) {
            return res.status(400).json({error: result.error.details[0].message});
        }

        const postData = {
            name: req.body.name,
            email: req.body.email,
            isGold: req.body.isGold,
            phone: req.body.phone
        };

        // Save the data in the database
        const customer = await Customer.create(postData);

        res.status(200).json({data: customer});
    } catch(err) {
        res.status(400).json({error: "Error in saving the document "+ err.message});
    }
});

/**
 * Function to update the existing customer
 */
router.put('/:id', async (req, res) => {
    try{
        // Validate the data
        const valid = validateData(req.body);

        if(valid.error) {
            return res.status(400).json({error: valid.error.details[0].message});
        }

        // Check if existing data exists in the database
        const customer = await Customer.findById(req.params.id);

        if(! customer) {
            res.status(400).json({error: "Data not found"});
        }

        const result = await Customer.findByIdAndUpdate(req.params.id, {name: req.body.name, slug: req.body.slug}, {new: true});

        res.status(200).json({data: result});
    } catch(err) {
        res.status(400).json({error: "Error in updating the document "+ err.message});
    }
});

/**
 * Function to delete the existing customer
 */
router.delete('/:id', async (req, res) => {
    try{
        // Check if existing data exists in the database
        const customer = await Customer.findById(req.params.id);

        if(! customer) {
            res.status(400).json({error: "Data not found"});
        }

        // Delete the document from the database
        const result = await Customer.findByIdAndDelete(req.params.id);

        // Return the deleted document
        res.status(200).json({data: result});
    } catch(err) {
        res.status(400).json({error: "Error in deleting the document "+ err.message});
    }
});


module.exports = router;