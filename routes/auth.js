const _ = require('lodash')

const {User} = require('../models/user');

const express = require('express');

const Joi = require('joi');

const bcrypt = require('bcrypt');

const router = express.Router();


router.post('/', async (req, res) => {
    try {
        // Validate the input data
        const validResult = validateUser(req.body);

        if(validResult.error) {
            res.status(400).json({error: validResult.error})
        }

        // Find if user already exists
        const user = await User.findOne({email: req.body.email})

        if(!user) res.status(400).json({error: 'Invalid email or password.'})
        
        const validPswd = await bcrypt.compare(req.body.password, user.password)

        if(!validPswd) res.status(400).json({error: 'Invalid email or password.'})
        
        const token = user.generateAuthToken()

        res.status(200).json({data: token});
    } catch(err) {
        res.status(400).json({error: err.message})
    }
})

function validateUser(data)
{
    // Validate the input data
    const schema = {
        email: Joi.string().required().email(),
        password: Joi.string().required()
    }

    const result = Joi.validate(data, schema);

    return result;
}

module.exports = router 