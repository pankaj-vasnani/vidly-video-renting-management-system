module.exports = (validator) => {
    return (req, res, next) => {
        // Validate the data
        const { err } = validator(req.body)

        if(err) return res.status(400).send(err.details[0].message)

        next()
    }
}