const jwt = require('jsonwebtoken')

const config = require('config')

module.exports = function authenticate(req, res, next) {
    const authToken = req.header('x-auth-token')

    if(!authToken) {
        res.status(401).json({error: 'Access Denied. No token provided'})
    }

    try {
        const decoded = jwt.verify(authToken, config.get('jwtprivatekey'))

        req.user = decoded

        next()
    } catch(ex) {
        res.status(400).json({error: 'Invalid token'})
    }

}