module.exports.absolute = function(number) {
    if(number >= 0) return number;
    
    return -number;
}

module.exports.greet = function(name) {
    return "Hello " + name + '!';
}

module.exports.currencies = function(name) {
    return ['USD', 'INR', 'IDR']
}

module.exports.getProduct = function() {
    return {id: 1, name: 'Test Product', price: 100, slug: 'test-product'}
}

module.exports.registeruser = function(username) {
    if(! username) throw new error("Username not given")

    return {username: username, id: 1}
}

module.exports.fizzbuzz = function(input) {
    if(typeof input !== 'number')
        throw new Error('Input should be a number')

    if(input < 0)
        throw new Error('Input must be a positive number')

    if((input % 3 == 0) && (input % 5 == 0))
        return 'FizzBuzz'

    if(input % 3 == 0)
        return 'Fizz'

    if(input % 5 == 0)
        return 'Buzz'

    return input


}

