const request = require('supertest')

const {Genre} = require('../../models/genre')

const {User} = require('../../models/user')

const mongoose = require('mongoose')

let server;

describe('/api/genres', () => {
    beforeEach(() => {
        server = require('../../index')
    })

    afterEach(async () => {
        await server.close();

        await Genre.remove({}) 
    })

    describe('GET /genres', () => {
        it('should return all the genres', async () => {
            await Genre.collection.insertMany([
                {name: 'genre1', slug: 'genre1'},
                {name: 'genre2', slug: 'genre2'}
            ]);

            const response = await request(server).get('/api/genres');

            expect(response.status).toBe(200);
            expect(response.body.data.length).toBe(response.body.data.length);
        })
    });

    describe('GET /genres/:id', () => {
        it('should return the genre corresponding to the particular id', async () => {
            const genre = new Genre({name: 'genre3', slug: 'genre3'})

            genre.save();

            const response = await request(server).get('/api/genres/'+genre._id)

            expect(response.status).toBe(200);
            expect(response.body.data).toHaveProperty('name', genre.name)
        })

        it('should return 404 if invalid object id is passed', async () => {
            const response = await request(server).get('/api/genres/1')
 
            expect(response.status).toBe(404);
        })
    });

    let token, id;
    let name, slug;

    const execPostGenre = async () => {
        return await request(server).post('/api/genres').set('x-auth-token', token).send({name, slug});
    }

    beforeEach(() => {
        name = 'genre 1';
        slug = 'genre-1';
        token = new User().generateAuthToken();
    });

    describe('POST /api/genres', () => {
        it('should return a 401 error if client is not logged in', async () => {
            token = ''

            const res = await execPostGenre();

            expect(res.status).toBe(401);
        })

        it('should return 400 if invalid genre data is sent or genre name is less than 3 characters', async () => {
            name = 'ge';

            const res = await execPostGenre()

            expect(res.status).toBe(400);
        })

        it('should return 400 if invalid genre data is sent or genre name is more than 50 characters', async () => {
            // const token = new User().generateAuthToken();
            name = new Array(52).join('a')

            const res = await execPostGenre()

            expect(res.status).toBe(400);
        })

        it('should save the genre if it is valid', async () => {
            await execPostGenre()

            const genre = await Genre.find({name: 'genre5'})

            expect(genre).not.toBeNull();
        });

        it('should return the genre in the body of the response', async () => {
            const res = await execPostGenre()
            
            expect(res.body.data).toHaveProperty('_id')
            expect(res.body.data).toHaveProperty('name', 'genre 1')
        });
    })

    const execDeleteGenre = async () => {
        return await request(server).delete('/api/genres/'+id).set('x-auth-token', token);
    }

    describe('DELETE /api/genres/:id', () => {
        it('should return 401 if the client is not logged in', async () => {
            token = ''

            const res = await execPostGenre();

            expect(res.status).toBe(401);
        })

        it('should return 400 if the genre data is not found', async () => {
            id = mongoose.Types.ObjectId().toHexString()

            const res = await execDeleteGenre()

            expect(res.status).toBe(400) 
        })

        it('should return 204 if genre is deleted', async () => {

            const genre = new Genre({name: 'delete genre', slug: 'delete-genre'})

            genre.save()

            id = genre._id

            const res = await execDeleteGenre()

            expect(res.status).toBe(200)
        })
    })

    const execPutGenre = async () => {
        return await request(server).put('/api/genres/'+id).set('x-auth-token', token).send({name, slug});
    }

    describe('PUT /api/genres/:id', () => {
        it('should return 401 if the client is not logged in', async () => {
            token = ''

            const res = await execPutGenre()

            expect(res.status).toBe(401)
        })

        it('should return 400 if the genre data is not found', async () => {
            id = mongoose.Types.ObjectId().toHexString()
            
            name = 'update genre again'
            slug = 'update-genre-again'

            const res = await execPutGenre()

            expect(res.status).toBe(400)
        })

        it('should return 400 if invalid genre data is sent or genre name is less than 3 characters', async () => {
            name = 'ge';

            const res = await execPutGenre()

            expect(res.status).toBe(400);
        })

        it('should return 400 if invalid genre data is sent or genre name is more than 50 characters', async () => {
            // const token = new User().generateAuthToken();
            name = new Array(52).join('a')

            const res = await execPutGenre()

            expect(res.status).toBe(400);
        })

        it('should return 200 if the genre successfully update', async () => {
            let genre = new Genre({ name: 'genre update', slug: 'genre-update'})

            genre.save();

            name = 'update genre again'
            slug = 'update-genre-again'

            id = genre._id

            const res = await execPutGenre()

            expect(res.status).toBe(200);
        })
    })
})