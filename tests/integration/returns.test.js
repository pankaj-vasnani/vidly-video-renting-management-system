const request = require('supertest')

const {Rental} = require('../../models/rental')
const {Movie} = require('../../models/movie')

const {User} = require('../../models/user')

const mongoose = require('mongoose')

const moment = require('moment')

let movie, server, customerId, movieId, rental, token

const exec = () => {
    return request(server).post('/api/returns').set('x-auth-token', token).send({customerId, movieId})
}

beforeEach(async () => {

    server = require('../../index')

    token = new User().generateAuthToken()

    customerId = mongoose.Types.ObjectId().toHexString()
    movieId    = mongoose.Types.ObjectId().toHexString()

    movie = new Movie({
        _id: movieId,
        title: 'Shikara',
        dailyRentalRate: 100,
        numberInStock: 11,
        genre:{
            name: 'Action',
            slug: 'action'
        }
    })

    await movie.save()

    rental = new Rental({
        customer: {
            _id: customerId,
            name: 'Pankaj',
            email: 'pankaj@gmail.com',
            isGold: true,
            phone: '434343434345945'
        },
        movie: {
            _id: movieId,
            title: 'Malang',
            dailyRentalRate: 10000
        }
    })

    await rental.save()
})

afterEach(async () => {
    await Rental.remove({})
    await Movie.remove({})
    await server.close();
})

describe('/api/returns', () => {
    it('should return 401 if the client is not logged in', async () => {
        token = ''

        const res = await exec()

        expect(res.status).toBe(401)
    })

    // it('should return 400 if customerId is not provided', async () => {
    //     customerId = ''

    //     const res = await exec()

    //     expect(res.status).toBe(400)
    // })

    // it('should return 400 if movieId is not provided', async () => {

    //     movieId = ''

    //     const res = await exec()

    //     expect(res.status).toBe(400)
    // })

    it('should return 404 if no rental is found for the customer/movie', async () => {
        await Rental.remove({})

        const res = await exec()

        expect(res.status).toBe(404)
    })

    it('should return 400 if the rental data already processed', async () => {
        rental.dateReturned = new Date()
        await rental.save()

        const res = await exec()

        expect(res.status).toBe(400)
    })

    it('should return 200 if the request is valid', async () => {
        const res = await exec()

        expect(res.status).toBe(200)
    })

    it('should set the returndate if the request data is valid', async () => {

        const res = await exec()

        const result = await Rental.findById(rental._id)

        const diffInTime = new Date() - result.dateReturned

        expect(diffInTime).toBeLessThan(10*1000)
    })

    it('should calculate the rental fee if the request data is valid', async () => {
        // Set the dateout variable in advance before creating new rental object 
        rental.dateOut = moment().add(-7, 'days').toDate();
        await rental.save()

        const res = await exec()

        const result = await Rental.findById(rental._id)

        expect(result.rentalPrice).toBeDefined()
    })

    it('should increase the movie stock', async () => {
        const res = await exec()

        const result = await Movie.findById(movie._id)

        expect(result.numberInStock).toBe(movie.numberInStock + 1)
    })

    it('should return the rental price', async () => {
        const res = await exec()

        expect(Object.keys(res.body.data)).toEqual(expect.arrayContaining(['_id', 'dateOut', 'dateReturned', 'customer', 'movie', 'rentalPrice']))
    })
})