const request = require('supertest')
let server;
let token;
const {User} = require('../../models/user')
const {Genre} = require('../../models/genre')

beforeEach(() => {
    server = require('../../index')
    token = new User().generateAuthToken()
})

afterEach(async () => {
    await server.close()

    await Genre.remove({})
})

const exec = () => {
    return request(server)
        .post('/api/genres')
        .set('x-auth-token', token)
        .send({name: 'genre1', slug: 'genre-1'})
}

describe('auth middleware', () => {
    it('should return 401 if no token is provided', async () => {
        token = ''

        const res = await exec();

        expect(res.status).toBe(401)
    })

    it('should return 400 if invalid token is provided', async () => {
        token = 'adsd'

        const res = await exec()

        expect(res.status).toBe(400)
    })

    it('should return 200 if valid token is provided', async () => {
        const res = await exec()

        expect(res.status).toBe(200)
    })
})