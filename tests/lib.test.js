// const TestRunner = require("jest-runner");
const lib = require('./lib');

describe('absolute', () => {
    it('Our first test', () => {
        const result = lib.absolute(1);

        expect(result).toBe(1);
    });

    it('Our second test', () => {
        const result = lib.absolute(-1);

        expect(result).toBe(1);
    });

    it('Our third test', () => {
        const result = lib.absolute(0);

        expect(result).toBe(0);
    });
})

describe('greet', () => {
    it('Our first test', () => {
        const result = lib.greet('Pankaj');

        expect(result).toMatch(/Pankaj/);
        expect(result).toContain('Pankaj');
    });
})

describe('array', () => {
    it('Our first test', () => {
        const result = lib.currencies();

        // TOO GENERAL
        // expect(result).toBeDefined();
        // expect(result).not.toBeNull();

        // TOO SPECIFIC
        // expect(result[0]).toBe('USD');
        // expect(result[1]).toBe('INR');
        // expect(result[2]).toBe('IDR');
        // expect(result.length).toBe(3);

        // proper way
        // expect(result).toContain('USD')
        // expect(result).toContain('INR')
        // expect(result).toContain('IDR')

        // IDEAL WAY
        expect(result).toEqual(expect.arrayContaining(['INR', 'IDR', 'USD']))
    });
})

describe('object', () => {
    it('Our first test', () => {
        const result = lib.getProduct();

        // expect(result).toEqual({id: 1, name: 'Test Product'})
        expect(result).toMatchObject({id: 1, name: 'Test Product'})

        expect(result).toHaveProperty('id', 1)
    });
})

describe('register', () => {
    it('Our first test', () => {
        const args = [null, undefined, NaN, '', 0, false]

        args.forEach(a => {
            expect(() => { lib.registeruser(a) }).toThrow()
        })
    });

    it('second test', () => {
        const result = lib.registeruser('pankaj')

        expect(result).toMatchObject({id: 1})
    });
})

describe('fizzbuzz test', () => {
    it('first test', () => {
        expect(() => { lib.fizzbuzz('test') }).toThrow()
        expect(() => { lib.fizzbuzz(null) }).toThrow()
        expect(() => { lib.fizzbuzz(undefined) }).toThrow()
        expect(() => { lib.fizzbuzz('') }).toThrow()
    })

    it('second test', () => {
        const result = lib.fizzbuzz(5)

        expect(result).toBe('Buzz')
    })

    it('third test', () => {
        const result = lib.fizzbuzz(3)
        
        expect(result).toBe('Fizz')
    })

    it('fourth test', () => {
        const result = lib.fizzbuzz(15)
        
        expect(result).toBe('FizzBuzz')
    })

    it('fifth test', () => {
        expect(() => {lib.fizzbuzz(-4)}).toThrow()
    })
})