const mongoose = require('mongoose');

const Joi = require('joi')

const moment = require('moment')

const schema = new mongoose.Schema({
    customer: {
        type: new mongoose.Schema({
            name: {
                type: String,
                required: true
            },
            email: {
                type: String,
                required: true
            },
            isGold: {
                type: Boolean,
                required: true
            },
            phone: {
                type: String,
                required: true
            }
        }),
        required: true
    },
    movie: {
        type: new mongoose.Schema({
            title: {
                type: String,
                required: true
            },
            dailyRentalRate: {
                type: Number,
                required: true
            }
        }),
        required: true
    },
    dateOut: {
        required: true,
        type: Date,
        default: Date.now
    },
    dateReturned: {
        type: Date
    },
    rentalPrice: {
        type: Number
    } 
})

schema.statics.lookup = function(customerId, movieId) {
    return this.findOne({
        'customer._id': customerId, 
        'movie._id': movieId
    })
}

schema.methods.return = function() {
    this.dateReturned = new Date()

    this.rentalPrice = moment().diff(this.dateOut, 'days') * this.movie.dailyRentalRate

}

const Rental = mongoose.model('Rental', schema);



function validateData(data) {
    // Validate the input data
    const schema = {
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required(),
        rentalPrice: Joi.number().required()
    }

    const result = Joi.validate(data, schema);

    return result;
}

module.exports.validate = validateData;
module.exports.Rental = Rental;