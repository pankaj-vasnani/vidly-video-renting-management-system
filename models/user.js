const mongoose = require('mongoose');

const jwt = require('jsonwebtoken');

const config = require('config');

const Joi = require('joi')


const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 10,
        max: 50
    },
     email: {
         type: String,
         required: true,
         unique: true
     },
     password: {
         type: String,
         required: true
     },
     isAdmin: Boolean
})

schema.methods.generateAuthToken = function() {
    const token = jwt.sign({_id: this._id, isAdmin: this.isAdmin}, config.get('jwtprivatekey'))

    return token
}

const User = mongoose.model('User', schema);


function validateData(data)
{
    // Validate the input data
    const schema = {
        name: Joi.string().min(10).max(50).required(),
        email: Joi.string().required().email(),
        password: Joi.string().required()
    }

    const result = Joi.validate(data, schema);

    return result;
}



module.exports.User = User
module.exports.validate = validateData