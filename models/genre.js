const mongoose = require('mongoose'); 

const Joi = require('Joi');

// Model Schema
const genreSchema = new mongoose.Schema({
    name: {type: String, minlength: 3, maxlength: 50, required: true},
    slug: {type: String, required: true}
});

// Generate model
const Genre = mongoose.model('Genre', genreSchema); 

/**
 * Function to validate the data from the incoming user request
 */
function validateData(data) {
    // Validate the input data
    const schema = {
        name: Joi.string().min(3).max(50).required(),
        slug: Joi.string().required()
    }

    const result = Joi.validate(data, schema);

    return result;
}

module.exports.Genre = Genre;
module.exports.validateData = validateData;
module.exports.genreSchema = genreSchema;

