const mongoose = require('mongoose');
const { genreSchema } = require('./genre');

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        min: 5,
        max: 255
    },
    genre: {
        type: genreSchema,
        required: true
    },
    numberInStock: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    }
});

const Movie = mongoose.model('Movie', schema);

/**
 * Function to validate the data from the incoming user request
 */
function validateData(data) {
    // Validate the input data
    const schema = {
        title: Joi.string().min(5).max(255).required(),
        genreId: Joi.objectId().required(),
        numberInStock: Joi.number().min(0).max(255).required(),
        dailyRentalRate: Joi.number().min(0).max(255).required()
    }

    const result = Joi.validate(data, schema);

    return result;
}

module.exports.Movie        = Movie
module.exports.validateData = validateData