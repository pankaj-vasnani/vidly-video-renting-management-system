const mongoose = require('mongoose');

const custSchema = mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    isGold: {type: Boolean, required: true},
    phone: {type: String, required: true}
});

const Customer = mongoose.model('Customer', custSchema);

/**
 * Function to validate the data from the incoming user request
 */
function validateData(data) {
    // Validate the input data
    const schema = {
        name: Joi.string().min(3).required(),
        email: Joi.required(),
        isGold: Joi.required(),
        phone: Joi.required()
    }

    const result = Joi.validate(data, schema);

    return result;
}

exports.Customer = Customer;
exports.validateData = validateData;