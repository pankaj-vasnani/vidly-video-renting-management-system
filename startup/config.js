const config = require('config')

module.exports = function() {
    if(! config.get('jwtprivatekey')) {
        throw new Error("JWT Private Key not defined")
    }   
}