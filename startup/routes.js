const express = require('express');
const genres = require('../routes/genre');
const customers = require('../routes/customer');
const movies = require('../routes/movie');
const rentals = require('../routes/rental');
const users = require('../routes/user');
const auth = require('../routes/auth');
const returns = require('../routes/returns');
const error = require('../middleware/error')

module.exports = function(app) {
    // Middleware for parsing the request body in json format
    app.use(express.json());

    // Routes for Genres API
    app.use('/api/genres', genres);

    // Routes for Customers API
    app.use('/api/customers', customers);

    // Routes for Movies API
    app.use('/api/movies', movies);

    // Routes for Rentals API
    app.use('/api/rentals', rentals);

    // Routes for Users API
    app.use('/api/users', users);

    // Routes for Auth API
    app.use('/api/auth', auth);

    // Routes for Returns API
    app.use('/api/returns', returns);

    // Middleware for Showing Error
    app.use(error)
}