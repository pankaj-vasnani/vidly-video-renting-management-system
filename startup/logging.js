require('express-async-errors')
const winston = require('winston')
// require('winston-mongodb')

logUncaughtExceptions = () => {
    winston.handleExceptions(
        new winston.transports.Console({colorsize: true, prettyPrint: true}),
        new winston.transports.File({filename: 'uncaughtExceptions.log'})
    )

    process.on('unhandledRejection', (ex) => {
        throw ex
    })
} 

logInfo = () => {
    winston.add(new winston.transports.File({filename: 'logfile.log'}))
}

module.exports.logUncaughtExceptions    = logUncaughtExceptions
module.exports.logInfo                  = logInfo