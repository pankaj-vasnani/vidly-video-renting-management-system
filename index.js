const winston = require('winston')
const express = require('express');
const app = express();
const {logInfo, logUncaughtExceptions} = require('./startup/logging')
require('./startup/routes')(app)
require('./startup/db')()
require('./startup/validation')
require('./startup/prod')(app)

// Handle Uncaught Exceptions
logUncaughtExceptions()

// Handle Application Logs
logInfo()

// Require Start Up Configuration for the Application
require('./startup/config')()

const port = process.env.PORT || 3000;

// Listening on port
const server = app.listen(port, () => {
    winston.info(`Listening on port ${port}`);
})

module.exports = server